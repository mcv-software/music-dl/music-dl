��    e      D              l  h   m     �  	   �  	   �  
   �     �     
  	     
   !     ,     2     8  
   A     L  	   T     ^     j     �  "   �  O   �                    #     )     2     G     f     ~  "   �     �  :   �     	     	     	     5	  
   J	     U	     ]	  (   b	     �	  ^   �	     �	     �	     
  	   #
     -
     6
     :
     R
     g
     l
     
     �
     �
  	   �
     �
     �
     �
  
   �
     �
     �
  ,   �
  	             "     (     8     @     E     T     \     m          �     �     �     �     �  
   �  U   �     -  �   2  P   �  F   P  �   �          $  /   D     t     w     �  	   �     �  *   �  c   �  a   F     �     �  
   �  �  �  �   �     F     T     b     n     ~     �     �     �  '   �     �               9     L     S  "   h     �  L   �  �   �     u  ;   �     �     �     �  #   �  /        >  (   [  U   �  $   �  �   �     �  5   �  ;   �          %  
   ,     7  ]   D     �  �   �     �  !   �     �     �  $        +  "   I  [   l     �     �  '   �     '     F     T     c     q     �  5   �  
   �     �  `     )   h  )   �     �  #   �  
   �     �  $     
   0  (   ;     d     w      �     �     �  ,   �       0     �   L     �  w  
  �   �  i      �   z      !  Z   !!  <   |!     �!     �!  %   �!  %   "  -   :"  A   h"  �   �"  �   \#     C$     ^$  
   p$    Is an application that will allow you to download music from popular sites such as youtube, zaycev.net. %d day,  %d days,  %d hour,  %d hours,  %d minute,  %d minutes,  %s second %s seconds &Next &Play &Results &Search in &Volume About {0} Application Audio Files(*.mp3)|*.mp3 Audio quality Avoid transcoding when downloading Briefly describe what happened. You will be able to thoroughly explain it later Cancel Check for updates Close Done! Download Download in Progress Downloading the new version... Downloading {0} ({1}%). Downloading {0}. Email address (Will not be public) Enable this service Enable this service (works only in the Russian Federation) Error Error playing {0}. {1}. Error while reporting File downloaded: {0} First Name General Help Here, you can describe the bug in detail High I know that the {0} bug system will get my email address to contact me and fix the bug quickly Include albums Include compilations Include singles Last Name Lossless Low Manuel Cortez (Spanish) Max results per page Mute New version for %s Next No results found.  Output device P&osition Password Pause Play Play/Pause Player Playing {0}. Please wait while your report is being send. Pre&vious Previous Ready Report an error S&earch Save Save this file Sear&ch Search by artist Searching {0}...  Send report Sending report... Services Settings Showing {0} results. Shuffle Shuffle on Something unexpected occurred while trying to report the bug. Please, try again later Stop Thanks for reporting this bug! In future versions, you may be able to find it in the changes list. You have received an email with more information regarding your report. You've reported the bug number %i The update has been downloaded and installed successfully. Press OK to continue. There was an error while trying to access the file you have requested. There's a new {app_name} version available. Would you like to download it now?

 {app_name} version: {app_version}

Changes:
{changes} Tidal Tidal username or email address Updating... {total_transferred} of {total_size} VK Visit website Volume down Volume up What's new in this version? You can subscribe for a tidal account here You must fill out the following fields: first name, last name, email address and issue information. You need to mark the checkbox to provide us your email address to contact you if it is necessary. Youtube Settings reported zaycev.net Project-Id-Version:  
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2020-07-23 05:41-0500
PO-Revision-Date: 2020-07-30 05:13-0500
Last-Translator: Manuel Cortez <manuel@manuelcortez.net>
Language: ru
Language-Team: ru <LL@li.org>
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.8.0
  Это приложение, позволяющее скачивать музыку с таких популярных сайтов как Youtube, zaycev.net. %d день,  %d дней,  %d час,  %d часов,  %d минута,  %d минут,  %s секунда %s секунд Следующая композиция Воспроизвести Результаты Искать с помощью Громкость О {0} Приложение Аудио Файлы(*.mp3)|*.mp3 Качество аудио Избегайте транскодирования при загрузке. Кратко опишите, что произошло. Позже Вы сможете описать это подробнее. Отмена Проверить на наличие обновлений Закрыть Готово! Скачать Процесс скачивания Скачивание новой версии... Загрузка {0} ({1}%). Сейчас загружается {0}. Адрес электронной почты (не будет опубликован) включить эту услугу Включить эту услугу (работает только на территории Российской Федерации). Ошибка Ошибка воспроизведения {0}. {1}. Ошибка во время отправки отчёта. Файл загружен: {0} Имя Общие Помощь Здесь Вы можете подробно описать возникшую ошибку. Высокое качество Я знаю, что {0} система ошибки получит мой адрес электронной почты, чтобы связаться со мной и быстро исправить ошибку. Включить альбомы Включить сборники Включить синглы Фамилия Качество без потерь Низкое качество Manuel Cortez (Испанский) Максимальное количество результатов на странице. Выключить звук Новая версия %s Следующая композиция Нет результатов. Output device Позиция Пароль: Приостановить Воспроизвести Воспроизвести/Приостановить Плеер Сейчас играет {0}. Пожалуйста, подождите, пока Ваш отчёт отправляется... Предыдущая композиция Предыдущая композиция Готово Сообщить об ошибке. Поиск Сохранить Сохранить этот файл Поиск Поиск по исполнителям Поиск {0}...  Отправить отчёт. Отправка отчёта... Сервисы Настройки Показано {0} результатов. Перемешать Случайный порядок включён Что-то неожиданное произошло при попытке сообщить об ошибке. Пожалуйста, попытайтесь позже. Остановить Спасибо за сообщение об этой ошибке. В будущих версиях Вы сможете найти его в списке изменений. Вы получили письмо на электронный адрес с подробной информацией о Вашем отчёте. Вы сообщили об ошибке номер %i. Обновление было успешно загружено и установлено. Нажмите ОК для продолжения. Произошла ошибка при попытке открыть запрашиваемый файл. Доступна новая версия {app_name}. Желаете скачать её?

 {app_name} версия: {app_version}

Изменения:
{changes} Tidal Имя пользователя Tidal или адрес электронной почты: Обновление...  {total_transferred} of {total_size} "ВКонтакте" Посетить вебсайт Уменьшить громкость Увеличить громкость Что нового в этой версии? Вы можете создать Tidal аккаунт здесь: Вы должны заполнить следующие поля: имя, фамилия, адрес электронной почты и информацию об ошибке. Необходимо отметить флажок, чтобы предоставить нам Ваш адрес электронной почты, чтобы связаться с Вами, если это понадобится. Настройки Youtube Сообщено! zaycev.net 